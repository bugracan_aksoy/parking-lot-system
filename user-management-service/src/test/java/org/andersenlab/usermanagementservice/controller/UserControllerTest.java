package org.andersenlab.usermanagementservice.controller;

import org.andersenlab.usermanagementservice.entity.AuthenticationRequest;
import org.andersenlab.usermanagementservice.entity.User;
import org.andersenlab.usermanagementservice.facade.UserFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

class UserControllerTest {

    @Mock
    private UserFacade userFacade;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testRegisterUser() {
        User user = new User();
        user.setUsername("testUser");
        user.setPassword("testPass");

        when(passwordEncoder.encode(user.getPassword())).thenReturn("encodedPass");
        when(userFacade.registerUser(any(User.class))).thenReturn(Map.of("user", user, "token", "token"));

        ResponseEntity<?> response = userController.registerUser(user);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(userFacade, times(1)).registerUser(any(User.class));
    }

    @Test
    void testLoginUser() {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest("testUser", "testPass");

        when(userFacade.loginUser(any(AuthenticationRequest.class))).thenReturn(Map.of("token", "token"));

        ResponseEntity<?> response = userController.loginUser(authenticationRequest);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(userFacade, times(1)).loginUser(any(AuthenticationRequest.class));
    }

    @Test
    void testGetAllUsers() {
        User user = new User();
        user.setUsername("testUser");

        Pageable pageable = PageRequest.of(0, 10);
        Page<User> pagedResponse = new PageImpl<>(Collections.singletonList(user), pageable, 1);

        when(userFacade.getAllUsers(pageable)).thenReturn(pagedResponse);

        ResponseEntity<Page<User>> response = userController.getAllUsers(0, 10);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().getTotalElements());
        verify(userFacade, times(1)).getAllUsers(pageable);
    }

    @Test
    void testGetUserById() {
        User user = new User();
        user.setId(1L);
        user.setUsername("testUser");

        when(userFacade.getUserById(1L)).thenReturn(user);

        ResponseEntity<User> response = userController.getUserById(1L);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(user, response.getBody());
        verify(userFacade, times(1)).getUserById(1L);
    }

    @Test
    void testDeleteUser() {
        doNothing().when(userFacade).deleteUser(anyLong());

        ResponseEntity<Void> response = userController.deleteUser(1L);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(userFacade, times(1)).deleteUser(anyLong());
    }
}
