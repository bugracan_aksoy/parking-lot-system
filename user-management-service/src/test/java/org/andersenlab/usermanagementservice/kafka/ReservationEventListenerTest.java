package org.andersenlab.usermanagementservice.kafka;

import org.andersenlab.usermanagementservice.entity.ReservationEvent;
import org.andersenlab.usermanagementservice.repository.ReservationEventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

class ReservationEventListenerTest {

    @Mock
    private ReservationEventRepository reservationEventRepository;

    @InjectMocks
    private ReservationEventListener reservationEventListener;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testHandleReservationCreated() {
        ReservationEvent reservationEvent = new ReservationEvent(1L, 1234L);

        reservationEventListener.handleReservationCreated(reservationEvent);

        verify(reservationEventRepository, times(1)).save(reservationEvent);
    }

    @Test
    void testHandleReservationCreatedException() {
        ReservationEvent reservationEvent = new ReservationEvent(1L, 1234L);

        doThrow(new RuntimeException("Error while saving reservation event")).when(reservationEventRepository).save(reservationEvent);

        reservationEventListener.handleReservationCreated(reservationEvent);

        verify(reservationEventRepository, times(1)).save(reservationEvent);
    }

    @Test
    void testHandleReservationUpdated() {
        ReservationEvent reservationEvent = new ReservationEvent(1L, 1234L);

        reservationEventListener.handleReservationUpdated(reservationEvent);

        verify(reservationEventRepository, times(1)).save(reservationEvent);
    }


    @Test
    void testHandleReservationDeleted() {
        ReservationEvent reservationEvent = new ReservationEvent(1L, 1234L);

        reservationEventListener.handleReservationDeleted(reservationEvent);

        verify(reservationEventRepository, times(1)).delete(reservationEvent);
    }

}
