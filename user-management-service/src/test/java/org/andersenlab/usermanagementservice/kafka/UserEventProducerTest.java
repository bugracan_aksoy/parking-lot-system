package org.andersenlab.usermanagementservice.kafka;

import org.andersenlab.usermanagementservice.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.Mockito.*;

class UserEventProducerTest {

    @Value("${kafka.topic.user.created}")
    private String USER_CREATED_TOPIC;

    @Value("${kafka.topic.user.updated}")
    private String USER_UPDATED_TOPIC;

    @Value("${kafka.topic.user.deleted}")
    private String USER_DELETED_TOPIC;

    @Mock
    private KafkaTemplate<String, Object> kafkaTemplate;

    @InjectMocks
    private UserEventProducer userEventProducer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void testSendUserCreatedEvent() {
        User user = new User();
        user.setUsername("testUser");

        userEventProducer.sendUserCreatedEvent(user);

        verify(kafkaTemplate, times(1)).send(USER_CREATED_TOPIC, user);
    }

    @Test
    void testSendUserCreatedEventException() {
        User user = new User();
        user.setUsername("testUser");

        doThrow(new RuntimeException("Error while sending event")).when(kafkaTemplate).send("user.created", user);

        userEventProducer.sendUserCreatedEvent(user);

        verify(kafkaTemplate, times(1)).send(USER_CREATED_TOPIC, user);
    }

    @Test
    void testSendUserUpdatedEvent() {
        User user = new User();
        user.setUsername("testUser");

        userEventProducer.sendUserUpdatedEvent(user);

        verify(kafkaTemplate, times(1)).send(USER_UPDATED_TOPIC, user);
    }

    @Test
    void testSendUserUpdatedEventException() {
        User user = new User();
        user.setUsername("testUser");

        doThrow(new RuntimeException("Error while sending event")).when(kafkaTemplate).send("user.updated", user);

        userEventProducer.sendUserUpdatedEvent(user);

        verify(kafkaTemplate, times(1)).send(USER_UPDATED_TOPIC, user);
    }

    @Test
    void testSendUserDeletedEvent() {
        Long userId = 1L;

        userEventProducer.sendUserDeletedEvent(userId);

        verify(kafkaTemplate, times(1)).send(USER_DELETED_TOPIC, userId);
    }

    @Test
    void testSendUserDeletedEventException() {
        Long userId = 1L;

        doThrow(new RuntimeException("Error while sending event")).when(kafkaTemplate).send("user.deleted", userId);

        userEventProducer.sendUserDeletedEvent(userId);

        verify(kafkaTemplate, times(1)).send(USER_DELETED_TOPIC, userId);
    }
}
