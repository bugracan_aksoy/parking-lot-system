package org.andersenlab.usermanagementservice.entity;

/* bugracanaksoy created on 28.05.2024 */

import jakarta.persistence.Embeddable;

@Embeddable
public class Car {

    private String make;
    private String model;
    private String year;

    public Car() {
    }

    public Car(String make, String model, String year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }

    // Getters and Setters
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}

