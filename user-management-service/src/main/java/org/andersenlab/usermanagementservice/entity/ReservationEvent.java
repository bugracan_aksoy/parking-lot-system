package org.andersenlab.usermanagementservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

/* bugracanaksoy created on 5.06.2024 */
@Entity
public class ReservationEvent {
    @Id
    private Long id;
    private Long userId;

    public ReservationEvent() {
    }


    public ReservationEvent(Long id, Long userId) {
        this.id = id;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
