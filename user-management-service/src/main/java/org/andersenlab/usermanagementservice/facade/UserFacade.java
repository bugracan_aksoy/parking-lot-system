package org.andersenlab.usermanagementservice.facade;

import org.andersenlab.usermanagementservice.entity.AuthenticationRequest;
import org.andersenlab.usermanagementservice.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/* bugracanaksoy created on 15.07.2024 */
public interface UserFacade {
    Map<String, Object> registerUser(User user);

    Map<String, Object> loginUser(AuthenticationRequest authenticationRequest);

    User updateUser(Long userId, User user);

    Page<User> getAllUsers(Pageable pageable);

    User getUserById(Long userId);

    void deleteUser(Long userId);
}
