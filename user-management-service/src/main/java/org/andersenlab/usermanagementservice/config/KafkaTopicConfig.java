package org.andersenlab.usermanagementservice.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

/* bugracanaksoy created on 8.07.2024 */
@Configuration
public class KafkaTopicConfig {

    @Value("${kafka.topic.user.created}")
    private String USER_CREATED_TOPIC;

    @Value("${kafka.topic.user.updated}")
    private String USER_UPDATED_TOPIC;

    @Value("${kafka.topic.user.deleted}")
    private String USER_DELETED_TOPIC;

    @Bean
    public NewTopic userCreatedTopic() {
        return TopicBuilder.name(USER_CREATED_TOPIC)
                .partitions(3)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic userUpdatedTopic() {
        return TopicBuilder.name(USER_UPDATED_TOPIC)
                .partitions(3)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic userDeletedTopic() {
        return TopicBuilder.name(USER_DELETED_TOPIC)
                .partitions(3)
                .replicas(1)
                .build();
    }
}
