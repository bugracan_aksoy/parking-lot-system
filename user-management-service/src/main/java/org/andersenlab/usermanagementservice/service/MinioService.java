package org.andersenlab.usermanagementservice.service;/* bugracanaksoy created on 15.07.2024 */

import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface MinioService {
    String uploadFile(MultipartFile file) throws Exception;

    InputStream getFile(String fileName) throws Exception;

    void deleteFile(String fileName) throws Exception;

    String updateFile(String existingFileName, MultipartFile newFile) throws Exception;
}
