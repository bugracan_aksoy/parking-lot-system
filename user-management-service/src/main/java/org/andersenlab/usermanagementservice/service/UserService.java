package org.andersenlab.usermanagementservice.service;/* bugracanaksoy created on 15.07.2024 */

import org.andersenlab.usermanagementservice.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

public interface UserService {
    @Transactional
    User registerUser(User user);

    @Transactional
    User updateUser(User user);

    Page<User> getAllUsers(Pageable pageable);

    User getUserById(Long id);

    @Transactional
    void deleteUser(Long id);
}
