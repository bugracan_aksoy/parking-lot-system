package org.andersenlab.usermanagementservice.service.impl;

import org.andersenlab.usermanagementservice.entity.User;
import org.andersenlab.usermanagementservice.kafka.UserEventProducer;
import org.andersenlab.usermanagementservice.repository.UserRepository;
import org.andersenlab.usermanagementservice.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/* bugracanaksoy created on 28.05.2024 */
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserEventProducer userEventProducer;

    private static final Logger logger = LogManager.getLogger(UserService.class);

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserEventProducer userEventProducer) {
        this.userRepository = userRepository;
        this.userEventProducer = userEventProducer;
    }

    @Override@Transactional
    public User registerUser(User user) {
        logger.info("Registering new user: {}", user.getUsername());
        try {
            User savedUser = userRepository.save(user);
            logger.info("Successfully registered user: {}", savedUser.getUsername());
            userEventProducer.sendUserCreatedEvent(savedUser);
            logger.info("User created event sent for user: {}", savedUser.getUsername());
            return savedUser;
        } catch (Exception e) {
            logger.error("Error registering user: {}", user.getUsername(), e);
            throw e;
        }
    }

    @Override@Transactional
    public User updateUser(User user) {
        logger.info("Updating user: {}", user.getUsername());
        try {
            User updatedUser = userRepository.save(user);
            logger.info("Successfully updated user: {}", updatedUser.getUsername());
            userEventProducer.sendUserUpdatedEvent(updatedUser);
            logger.info("User updated event sent for user: {}", updatedUser.getUsername());
            return updatedUser;
        } catch (Exception e) {
            logger.error("Error updating user: {}", user.getUsername(), e);
            throw e;
        }
    }

    @Override public Page<User> getAllUsers(Pageable pageable) {
        logger.info("Retrieving all users");
        try {
            Page<User> users = userRepository.findAll(pageable);
            logger.info("Successfully retrieved all users");
            return users;
        } catch (Exception e) {
            logger.error("Error retrieving all users", e);
            throw e;
        }
    }

    @Override public User getUserById(Long id) {
        logger.info("Retrieving user by ID: {}", id);
        try {
            User user = userRepository.findById(id).orElse(null);
            if (user != null) {
                logger.info("Successfully retrieved user with ID: {}", id);
            } else {
                logger.warn("User not found with ID: {}", id);
            }
            return user;
        } catch (Exception e) {
            logger.error("Error retrieving user by ID: {}", id, e);
            throw e;
        }
    }

    @Override@Transactional
    public void deleteUser(Long id) {
        logger.info("Deleting user with ID: {}", id);
        try {
            userRepository.deleteById(id);
            logger.info("Successfully deleted user with ID: {}", id);
            userEventProducer.sendUserDeletedEvent(id);
            logger.info("User deleted event sent for user ID: {}", id);
        } catch (Exception e) {
            logger.error("Error deleting user with ID: {}", id, e);
            throw e;
        }
    }
}
