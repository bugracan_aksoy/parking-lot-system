package org.andersenlab.usermanagementservice.repository;

import org.andersenlab.usermanagementservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/* bugracanaksoy created on 28.05.2024 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
