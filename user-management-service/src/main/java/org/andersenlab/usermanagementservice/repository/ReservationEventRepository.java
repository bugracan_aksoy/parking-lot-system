package org.andersenlab.usermanagementservice.repository;/* bugracanaksoy created on 5.06.2024 */

import org.andersenlab.usermanagementservice.entity.ReservationEvent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationEventRepository extends JpaRepository<ReservationEvent, Long> {
}
