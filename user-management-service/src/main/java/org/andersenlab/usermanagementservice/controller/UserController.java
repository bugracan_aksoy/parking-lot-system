package org.andersenlab.usermanagementservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.andersenlab.usermanagementservice.entity.AuthenticationRequest;
import org.andersenlab.usermanagementservice.entity.User;
import org.andersenlab.usermanagementservice.facade.UserFacade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/* bugracanaksoy created on 28.05.2024 */
@RestController
@RequestMapping("/users")
@Tag(name = "User Management System", description = "Operations pertaining to user management")
public class UserController {
    private final UserFacade userFacade;
    private static final Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Operation(summary = "Register a new user", description = "Register a new user and return a JWT token", tags = {"User Management System"})
    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Parameter(description = "User object to be registered", required = true) @RequestBody User user) {
        logger.info("Received request to register user: {}", user.getUsername());
        try {
            Map<String, Object> response = userFacade.registerUser(user);
            logger.info("Successfully registered user: {}", user.getUsername());
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            logger.error("Error registering user: {}", user.getUsername(), e);
            return ResponseEntity.status(500).body("Error registering user");
        }
    }

    @Operation(summary = "Login user", description = "Authenticate a user and return a JWT token", tags = {"User Management System"})
    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@Parameter(description = "Authentication request containing username and password", required = true) @RequestBody AuthenticationRequest authenticationRequest) {
        logger.info("Received login request for user: {}", authenticationRequest.getUsername());
        try {
            Map<String, Object> response = userFacade.loginUser(authenticationRequest);
            logger.info("User authenticated: {}", authenticationRequest.getUsername());
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            logger.error("Error logging in user: {}", authenticationRequest.getUsername(), e);
            return ResponseEntity.status(401).body("Invalid username or password");
        }
    }

    @Operation(summary = "Update an existing user", description = "Update user details", tags = {"User Management System"})
    @PutMapping("/{userId}")
    public ResponseEntity<User> updateUser(@Parameter(description = "ID of the user to be updated", required = true) @PathVariable Long userId, @Parameter(description = "Updated user object", required = true) @RequestBody User user) {
        logger.info("Received request to update user with ID: {}", userId);
        try {
            User updatedUser = userFacade.updateUser(userId, user);
            logger.info("Successfully updated user with ID: {}", userId);
            return ResponseEntity.ok(updatedUser);
        } catch (Exception e) {
            logger.error("Error updating user with ID: {}", userId, e);
            return ResponseEntity.status(500).body(null);
        }
    }

    @Operation(summary = "Get all users with pagination", description = "Retrieve all users with pagination support", tags = {"User Management System"})
    @GetMapping
    public ResponseEntity<Page<User>> getAllUsers(
            @Parameter(description = "Page number for pagination", example = "0") @RequestParam(defaultValue = "0") int page,
            @Parameter(description = "Page size for pagination", example = "10") @RequestParam(defaultValue = "10") int size) {
        logger.info("Received request to get all users with pagination - page: {}, size: {}", page, size);
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<User> users = userFacade.getAllUsers(pageable);
            logger.info("Successfully retrieved all users with pagination");
            return ResponseEntity.ok(users);
        } catch (Exception e) {
            logger.error("Error retrieving all users with pagination", e);
            return ResponseEntity.status(500).body(null);
        }
    }

    @Operation(summary = "Get user by ID", description = "Retrieve user details by user ID", tags = {"User Management System"})
    @GetMapping("/{userId}")
    public ResponseEntity<User> getUserById(@Parameter(description = "ID of the user to retrieve", required = true) @PathVariable Long userId) {
        logger.info("Received request to get user with ID: {}", userId);
        try {
            User user = userFacade.getUserById(userId);
            logger.info("Successfully retrieved user with ID: {}", userId);
            return ResponseEntity.ok(user);
        } catch (Exception e) {
            logger.error("Error retrieving user with ID: {}", userId, e);
            return ResponseEntity.status(500).body(null);
        }
    }

    @Operation(summary = "Delete user by ID", description = "Delete a user by ID", tags = {"User Management System"})
    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteUser(@Parameter(description = "ID of the user to delete", required = true) @PathVariable Long userId) {
        logger.info("Received request to delete user with ID: {}", userId);
        try {
            userFacade.deleteUser(userId);
            logger.info("Successfully deleted user with ID: {}", userId);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            logger.error("Error deleting user with ID: {}", userId, e);
            return ResponseEntity.status(500).build();
        }
    }
}
