package org.andersenlab.usermanagementservice.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/* bugracanaksoy created on 29.05.2024 */
@Controller
public class CustomErrorController implements ErrorController {

    private static final Logger logger = LogManager.getLogger(CustomErrorController.class);

    @RequestMapping("/error")
    public String handleError() {
        logger.error("An error occurred, handling it with custom error controller.");
        return "error";
    }
}
