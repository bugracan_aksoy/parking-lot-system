package org.andersenlab.usermanagementservice.kafka;

import org.andersenlab.usermanagementservice.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/* bugracanaksoy created on 29.05.2024 */
@Service
public class UserEventProducer {
    @Value("${kafka.topic.user.created}")
    private String USER_CREATED_TOPIC;

    @Value("${kafka.topic.user.updated}")
    private String USER_UPDATED_TOPIC;

    @Value("${kafka.topic.user.deleted}")
    private String USER_DELETED_TOPIC;

    private final KafkaTemplate<String, Object> kafkaTemplate;

    private static final Logger logger = LogManager.getLogger(UserEventProducer.class);

    @Autowired
    public UserEventProducer(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendUserCreatedEvent(User user) {
        logger.info("Sending user created event for user: {}", user.getUsername());
        try {
            kafkaTemplate.send(USER_CREATED_TOPIC, user);
            logger.info("Successfully sent user created event for user: {}", user.getUsername());
        } catch (Exception e) {
            logger.error("Error while sending user created event for user: {}", user.getUsername(), e);
        }
    }

    public void sendUserUpdatedEvent(User user) {
        logger.info("Sending user updated event for user: {}", user.getUsername());
        try {
            kafkaTemplate.send(USER_UPDATED_TOPIC, user);
            logger.info("Successfully sent user updated event for user: {}", user.getUsername());
        } catch (Exception e) {
            logger.error("Error while sending user updated event for user: {}", user.getUsername(), e);
        }
    }

    public void sendUserDeletedEvent(Long userId) {
        logger.info("Sending user deleted event for user ID: {}", userId);
        try {
            kafkaTemplate.send(USER_DELETED_TOPIC, userId);
            logger.info("Successfully sent user deleted event for user ID: {}", userId);
        } catch (Exception e) {
            logger.error("Error while sending user deleted event for user ID: {}", userId, e);
        }
    }
}
