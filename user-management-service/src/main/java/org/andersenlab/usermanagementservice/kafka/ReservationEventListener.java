package org.andersenlab.usermanagementservice.kafka;

import org.andersenlab.usermanagementservice.entity.ReservationEvent;
import org.andersenlab.usermanagementservice.repository.ReservationEventRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/* bugracanaksoy created on 29.05.2024 */
@Service
public class ReservationEventListener {

    private final ReservationEventRepository reservationEventRepository;

    private static final String RESERVATION_CREATED_TOPIC = "reservation.created";
    private static final String RESERVATION_UPDATED_TOPIC = "reservation.updated";
    private static final String RESERVATION_DELETED_TOPIC = "reservation.deleted";

    private static final Logger logger = LogManager.getLogger(ReservationEventListener.class);

    @Autowired
    public ReservationEventListener(ReservationEventRepository reservationEventRepository) {
        this.reservationEventRepository = reservationEventRepository;
    }

    @KafkaListener(topics = RESERVATION_CREATED_TOPIC, groupId = "user-service")
    public void handleReservationCreated(ReservationEvent reservationEvent) {
        logger.info("Received reservation created event: {}", reservationEvent);
        try {
            reservationEventRepository.save(reservationEvent);
            logger.info("Successfully saved reservation event: {}", reservationEvent);
        } catch (Exception e) {
            logger.error("Error while saving reservation event: {}", reservationEvent, e);
        }
    }

    @KafkaListener(topics = RESERVATION_UPDATED_TOPIC, groupId = "user-service")
    public void handleReservationUpdated(ReservationEvent reservationEvent) {
        logger.info("Received reservation updated event: {}", reservationEvent);
        try {
            reservationEventRepository.save(reservationEvent);
            logger.info("Successfully updated reservation event: {}", reservationEvent);
        } catch (Exception e) {
            logger.error("Error while updating reservation event: {}", reservationEvent, e);
        }
    }

    @KafkaListener(topics = RESERVATION_DELETED_TOPIC, groupId = "user-service")
    public void handleReservationDeleted(ReservationEvent reservationEvent) {
        logger.info("Received reservation deleted event: {}", reservationEvent);
        try {
            reservationEventRepository.delete(reservationEvent);
            logger.info("Successfully deleted reservation event: {}", reservationEvent);
        } catch (Exception e) {
            logger.error("Error while deleting reservation event: {}", reservationEvent, e);
        }
    }
}
