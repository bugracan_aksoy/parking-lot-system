# ParkingLot Management Microservices Project

## Project Overview

This project consists of two microservices: `user-management-service` and `reservation-service`. These services are designed to handle user management and reservation functionalities, respectively. The project is built using Java with Spring Boot and Docker for containerization.

## Services

### User Management Service

The `user-management-service` handles operations related to user management, such as creating, updating, and deleting user accounts.

#### Controller Methods

**Register User**
- **Endpoint**: `/users/register`
- **Method**: POST
- **Request Body**: `User` object
- **Description**: Registers a new user and returns the user object along with a JWT token.

**Login User**
- **Endpoint**: `/users/login`
- **Method**: POST
- **Request Body**: `AuthenticationRequest` object
- **Description**: Authenticates a user and returns a JWT token.

**Update User**
- **Endpoint**: `/users/{userId}`
- **Method**: PUT
- **Path Variable**: `userId` (Long)
- **Request Body**: `User` object
- **Description**: Updates an existing user.

**Get All Users**
- **Endpoint**: `/users`
- **Method**: GET
- **Request Params**: `page` (int, default: 0), `size` (int, default: 10)
- **Description**: Retrieves all users with pagination.

**Get User by ID**
- **Endpoint**: `/users/{userId}`
- **Method**: GET
- **Path Variable**: `userId` (Long)
- **Description**: Retrieves a user by ID.

**Delete User**
- **Endpoint**: `/users/{userId}`
- **Method**: DELETE
- **Path Variable**: `userId` (Long)
- **Description**: Deletes a user by ID.

### Reservation Service

The `reservation-service` handles operations related to reservations, such as creating, updating, and deleting reservations.

#### Controller Methods

**Create Reservation**
- **Endpoint**: `/reservations`
- **Method**: POST
- **Request Body**: `Reservation` object
- **Description**: Creates a new reservation and returns the created reservation.

**Get Reservation by ID**
- **Endpoint**: `/reservations/{reservationId}`
- **Method**: GET
- **Path Variable**: `reservationId` (Long)
- **Description**: Retrieves a reservation by ID.

**Get All Reservations**
- **Endpoint**: `/reservations`
- **Method**: GET
- **Request Params**: `page` (int, default: 0), `size` (int, default: 10)
- **Description**: Retrieves all reservations with pagination.

**Update Reservation**
- **Endpoint**: `/reservations/{reservationId}`
- **Method**: PUT
- **Path Variable**: `reservationId` (Long)
- **Request Body**: `Reservation` object
- **Description**: Updates an existing reservation.

**Delete Reservation**
- **Endpoint**: `/reservations/{reservationId}`
- **Method**: DELETE
- **Path Variable**: `reservationId` (Long)
- **Description**: Deletes a reservation by ID.

### Swagger Documentation

Both services have integrated Swagger UI for API documentation and testing. You can access the Swagger UI at the following URLs:

- **User Management Service**: [http://localhost:8080/swagger-ui.html](http://localhost:8081/swagger-ui.html)
- **Reservation Service**: [http://localhost:8090/swagger-ui.html](http://localhost:8082/swagger-ui.html)

### Architecture and Design Details

**Connection Types**:
- **Kafka**: For asynchronous messaging between microservices.
- **REST**: For synchronous communication and CRUD operations.

**Database**:
- **PostgreSQL**: Used for persisting user data.

**Design Patterns**:
- **Event-Driven Architecture**: Uses Kafka to publish and subscribe to user-related events.
- **Service Layer Pattern**: Business logic is encapsulated within service classes.
- **Repository Pattern**: Abstracts the data access layer.
- **Controller Pattern**: Manages user input via REST controllers.
- **Dependency Injection**: Uses Spring’s `@Autowired` for injecting dependencies.


## Prerequisites

Before you start, ensure you have the following installed:

- Java 11 or higher
- Docker
- Docker Compose
- Maven

## How to Start

### Building the Services

1. **Navigate to the project directory:**
    ```bash
    cd AndersenLab
    ```

2. **Build the User Management Service:**
    ```bash
    cd user-management-service
    mvn clean install
    cd ..
    ```

3. **Build the Reservation Service:**
    ```bash
    cd reservation-service
    mvn clean install
    cd ..
    ```

### Running the Services

You can run both services using Docker Compose.

1. **Navigate to the `user-management-service` directory:**
    ```bash
    cd user-management-service
    ```

2. **Start the services using Docker Compose:**
    ```bash
    docker-compose up
    ```

This command will start both the user management and reservation services.

