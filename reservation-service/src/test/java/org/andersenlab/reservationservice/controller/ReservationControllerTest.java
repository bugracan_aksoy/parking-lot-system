package org.andersenlab.reservationservice.controller;

import org.andersenlab.reservationservice.facade.ReservationFacade;
import org.andersenlab.reservationservice.model.Reservation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

class ReservationControllerTest {

    @Mock
    private ReservationFacade reservationFacade;

    @InjectMocks
    private ReservationController reservationController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testCreateReservation() {
        Reservation reservation = new Reservation();
        reservation.setId(1L);

        when(reservationFacade.createReservation(any(Reservation.class))).thenReturn(reservation);

        ResponseEntity<?> response = reservationController.createReservation(reservation);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(reservation, response.getBody());
        verify(reservationFacade, times(1)).createReservation(any(Reservation.class));
    }

    @Test
    void testGetAllReservations() {
        Reservation reservation = new Reservation();
        reservation.setId(1L);

        Pageable pageable = PageRequest.of(0, 10);
        Page<Reservation> pagedResponse = new PageImpl<>(Collections.singletonList(reservation), pageable, 1);

        when(reservationFacade.getAllReservations(pageable)).thenReturn(pagedResponse);

        ResponseEntity<Page<Reservation>> response = reservationController.getAllReservations(0, 10);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().getTotalElements());
        verify(reservationFacade, times(1)).getAllReservations(pageable);
    }

    @Test
    void testGetReservationById() {
        Reservation reservation = new Reservation();
        reservation.setId(1L);

        when(reservationFacade.getReservationById(1L)).thenReturn(reservation);

        ResponseEntity<Reservation> response = reservationController.getReservationById(1L);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(reservation, response.getBody());
        verify(reservationFacade, times(1)).getReservationById(1L);
    }

    @Test
    void testUpdateReservation() {
        Reservation reservation = new Reservation();
        reservation.setId(1L);

        when(reservationFacade.updateReservation(anyLong(), any(Reservation.class))).thenReturn(reservation);

        ResponseEntity<Reservation> response = reservationController.updateReservation(1L, reservation);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(reservation, response.getBody());
        verify(reservationFacade, times(1)).updateReservation(anyLong(), any(Reservation.class));
    }

    @Test
    void testDeleteReservation() {
        doNothing().when(reservationFacade).deleteReservation(anyLong());

        ResponseEntity<Void> response = reservationController.deleteReservation(1L);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(reservationFacade, times(1)).deleteReservation(anyLong());
    }
}
