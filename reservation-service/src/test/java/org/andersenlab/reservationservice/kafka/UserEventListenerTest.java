package org.andersenlab.reservationservice.kafka;

import org.andersenlab.reservationservice.model.UserEvent;
import org.andersenlab.reservationservice.repository.UserEventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

class UserEventListenerTest {

    @Mock
    private UserEventRepository userEventRepository;

    @InjectMocks
    private UserEventListener userEventListener;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testHandleUserCreated() {
        UserEvent userEvent = new UserEvent(1L, "testUser");

        userEventListener.handleUserCreated(userEvent);

        verify(userEventRepository, times(1)).save(userEvent);
    }

    @Test
    void testHandleUserCreatedException() {
        UserEvent userEvent = new UserEvent(1L, "testUser");

        doThrow(new RuntimeException("Error while saving user event")).when(userEventRepository).save(userEvent);

        userEventListener.handleUserCreated(userEvent);

        verify(userEventRepository, times(1)).save(userEvent);
    }

    @Test
    void testHandleUserUpdated() {
        UserEvent userEvent = new UserEvent(1L, "testUser");

        userEventListener.handleUserUpdated(userEvent);

        verify(userEventRepository, times(1)).save(userEvent);
    }

    @Test
    void testHandleUserDeleted() {
        UserEvent userEvent = new UserEvent(1L, "testUser");

        userEventListener.handleUserDeleted(userEvent);

        verify(userEventRepository, times(1)).delete(userEvent);
    }

}
