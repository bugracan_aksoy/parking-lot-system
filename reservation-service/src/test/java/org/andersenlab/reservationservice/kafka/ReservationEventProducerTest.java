package org.andersenlab.reservationservice.kafka;

import org.andersenlab.reservationservice.model.Reservation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.Mockito.*;

class ReservationEventProducerTest {

    @Value("${kafka.topic.reservation.created}")
    private String RESERVATION_CREATED_TOPIC;

    @Value("${kafka.topic.reservation.updated}")
    private String RESERVATION_UPDATED_TOPIC;

    @Value("${kafka.topic.reservation.deleted}")
    private String RESERVATION_DELETED_TOPIC;

    @Mock
    private KafkaTemplate<String, Object> kafkaTemplate;

    @InjectMocks
    private ReservationEventProducer reservationEventProducer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSendReservationCreatedEvent() {
        Reservation reservation = new Reservation();
        reservation.setId(1L);

        reservationEventProducer.sendReservationCreatedEvent(reservation);

        verify(kafkaTemplate, times(1)).send(RESERVATION_CREATED_TOPIC, reservation);
    }

    @Test
    void testSendReservationCreatedEventException() {
        Reservation reservation = new Reservation();
        reservation.setId(1L);

        doThrow(new RuntimeException("Error while sending event")).when(kafkaTemplate).send(RESERVATION_CREATED_TOPIC, reservation);

        reservationEventProducer.sendReservationCreatedEvent(reservation);

        verify(kafkaTemplate, times(1)).send(RESERVATION_CREATED_TOPIC, reservation);
    }

    @Test
    void testSendReservationUpdatedEvent() {
        Reservation reservation = new Reservation();
        reservation.setId(1L);

        reservationEventProducer.sendReservationUpdatedEvent(reservation);

        verify(kafkaTemplate, times(1)).send(RESERVATION_UPDATED_TOPIC, reservation);
    }

    @Test
    void testSendReservationUpdatedEventException() {
        Reservation reservation = new Reservation();
        reservation.setId(1L);

        doThrow(new RuntimeException("Error while sending event")).when(kafkaTemplate).send(RESERVATION_UPDATED_TOPIC, reservation);

        reservationEventProducer.sendReservationUpdatedEvent(reservation);

        verify(kafkaTemplate, times(1)).send(RESERVATION_UPDATED_TOPIC, reservation);
    }

    @Test
    void testSendReservationDeletedEvent() {
        Long reservationId = 1L;

        reservationEventProducer.sendReservationDeletedEvent(reservationId);

        verify(kafkaTemplate, times(1)).send(RESERVATION_DELETED_TOPIC, reservationId);
    }

    @Test
    void testSendReservationDeletedEventException() {
        Long reservationId = 1L;

        doThrow(new RuntimeException("Error while sending event")).when(kafkaTemplate).send(RESERVATION_DELETED_TOPIC, reservationId);

        reservationEventProducer.sendReservationDeletedEvent(reservationId);

        verify(kafkaTemplate, times(1)).send(RESERVATION_DELETED_TOPIC, reservationId);
    }
}
