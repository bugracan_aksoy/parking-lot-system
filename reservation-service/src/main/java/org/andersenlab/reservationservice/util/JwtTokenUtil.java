package org.andersenlab.reservationservice.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

/* bugracanaksoy created on 29.05.2024 */
@Component
public class JwtTokenUtil {

    @Value("${jwt.secret}")
    private String secret;

    private static final Logger logger = LogManager.getLogger(JwtTokenUtil.class);

    public String getUsernameFromToken(String token) {
        logger.info("Extracting username from token");
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getExpirationDateFromToken(String token) {
        logger.info("Extracting expiration date from token");
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        logger.info("Getting all claims from token");
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        boolean isExpired = expiration.before(new Date());
        logger.info("Token expired: {}", isExpired);
        return isExpired;
    }

    public Boolean validateToken(String token) {
        logger.info("Validating token");
        boolean isValid = !isTokenExpired(token);
        logger.info("Token valid: {}", isValid);
        return isValid;
    }
}
