package org.andersenlab.reservationservice.repository;

import org.andersenlab.reservationservice.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

/* bugracanaksoy created on 29.05.2024 */
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
}
