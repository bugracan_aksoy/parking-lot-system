package org.andersenlab.reservationservice.repository;/* bugracanaksoy created on 5.06.2024 */

import org.andersenlab.reservationservice.model.UserEvent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserEventRepository extends JpaRepository<UserEvent, Long> {
}
