package org.andersenlab.reservationservice.service.impl;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import org.andersenlab.reservationservice.service.GoogleCalendarService;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;

/* bugracanaksoy created on 10.06.2024 */
@Service
public class GoogleCalendarServiceImpl implements GoogleCalendarService {

    private static final String APPLICATION_NAME = "Car Parking Reservation Service";
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

    private GoogleCalendarServiceImpl() {
    }

    public static Calendar getCalendarService(Credential credential) throws GeneralSecurityException, IOException {
        return new Calendar.Builder(
                GoogleNetHttpTransport.newTrustedTransport(), JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    public static void addEventToCalendar(Credential credential, Event event) throws GeneralSecurityException, IOException {
        Calendar service = getCalendarService(credential);
        service.events().insert("primary", event).execute();
    }
}
