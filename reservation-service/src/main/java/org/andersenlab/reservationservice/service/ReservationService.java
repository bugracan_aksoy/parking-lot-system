package org.andersenlab.reservationservice.service;/* bugracanaksoy created on 15.07.2024 */

import org.andersenlab.reservationservice.model.Reservation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

public interface ReservationService {
    @Transactional
    Reservation createReservation(Reservation reservation);

    Reservation getReservationById(Long id);

    Page<Reservation> getAllReservations(Pageable pageable);

    @Transactional
    Reservation updateReservation(Reservation reservation);

    @Transactional
    void deleteReservation(Long id);
}
