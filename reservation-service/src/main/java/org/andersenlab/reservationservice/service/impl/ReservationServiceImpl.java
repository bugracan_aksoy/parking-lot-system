package org.andersenlab.reservationservice.service.impl;

import org.andersenlab.reservationservice.model.Reservation;
import org.andersenlab.reservationservice.kafka.ReservationEventProducer;
import org.andersenlab.reservationservice.repository.ReservationRepository;
import org.andersenlab.reservationservice.repository.UserEventRepository;
import org.andersenlab.reservationservice.service.ReservationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/* bugracanaksoy created on 29.05.2024 */
@Service
public class ReservationServiceImpl implements ReservationService {
    private final UserEventRepository userEventRepository;
    private final ReservationRepository reservationRepository;
    private final ReservationEventProducer reservationEventProducer;

    private static final Logger logger = LogManager.getLogger(ReservationService.class);

    @Autowired
    public ReservationServiceImpl(UserEventRepository userEventRepository, ReservationRepository reservationRepository, ReservationEventProducer reservationEventProducer) {
        this.userEventRepository = userEventRepository;
        this.reservationRepository = reservationRepository;
        this.reservationEventProducer = reservationEventProducer;
    }

    @Override@Transactional
    public Reservation createReservation(Reservation reservation) {
        logger.info("Creating reservation: {}", reservation);
        try {
            if(userEventRepository.existsById(reservation.getUserId())){
                Reservation createdReservation = reservationRepository.save(reservation);
                reservationEventProducer.sendReservationCreatedEvent(createdReservation);
                logger.info("Successfully created reservation: {}", createdReservation);
                return createdReservation;
            }else{
                throw new UsernameNotFoundException(reservation.getUserId().toString());
            }

        } catch (Exception e) {
            logger.error("Error creating reservation: {}", reservation, e);
            throw e;
        }
    }

    @Override public Reservation getReservationById(Long id) {
        logger.info("Retrieving reservation by ID: {}", id);
        try {
            Reservation reservation = reservationRepository.findById(id).orElse(null);
            if (reservation != null) {
                logger.info("Successfully retrieved reservation: {}", reservation);
            } else {
                logger.warn("Reservation not found with ID: {}", id);
            }
            return reservation;
        } catch (Exception e) {
            logger.error("Error retrieving reservation by ID: {}", id, e);
            throw e;
        }
    }

    @Override public Page<Reservation> getAllReservations(Pageable pageable) {
        logger.info("Retrieving all reservations");
        try {
            Page<Reservation> reservations = reservationRepository.findAll(pageable);
            logger.info("Successfully retrieved all reservations");
            return reservations;
        } catch (Exception e) {
            logger.error("Error retrieving all reservations", e);
            throw e;
        }
    }

    @Override@Transactional
    public Reservation updateReservation(Reservation reservation) {
        logger.info("Updating reservation: {}", reservation);
        try {
            Reservation updatedReservation = reservationRepository.save(reservation);
            reservationEventProducer.sendReservationUpdatedEvent(updatedReservation);
            logger.info("Successfully updated reservation: {}", updatedReservation);
            return updatedReservation;
        } catch (Exception e) {
            logger.error("Error updating reservation: {}", reservation, e);
            throw e;
        }
    }

    @Override@Transactional
    public void deleteReservation(Long id) {
        logger.info("Deleting reservation with ID: {}", id);
        try {
            reservationRepository.deleteById(id);
            reservationEventProducer.sendReservationDeletedEvent(id);
            logger.info("Successfully deleted reservation with ID: {}", id);
        } catch (Exception e) {
            logger.error("Error deleting reservation with ID: {}", id, e);
            throw e;
        }
    }
}
