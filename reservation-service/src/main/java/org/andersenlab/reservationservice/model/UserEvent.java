package org.andersenlab.reservationservice.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

/* bugracanaksoy created on 29.05.2024 */
@Entity
public class UserEvent {
    @Id
    private Long id;
    private String username;

    public UserEvent() {
    }

    public UserEvent(Long id, String username) {
        this.id = id;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}

