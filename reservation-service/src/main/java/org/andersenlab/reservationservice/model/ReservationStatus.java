package org.andersenlab.reservationservice.model;/* bugracanaksoy created on 5.06.2024 */

public enum ReservationStatus {
    PENDING,
    CONFIRMED,
    CANCELLED,
    COMPLETED,
    NO_SHOW;
}
