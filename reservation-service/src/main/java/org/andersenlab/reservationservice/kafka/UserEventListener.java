package org.andersenlab.reservationservice.kafka;

import org.andersenlab.reservationservice.model.UserEvent;
import org.andersenlab.reservationservice.repository.UserEventRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/* bugracanaksoy created on 29.05.2024 */
@Service
public class UserEventListener {

    private final UserEventRepository userEventRepository;

    private static final String USER_CREATED_TOPIC = "user.created";
    private static final String USER_UPDATED_TOPIC = "user.updated";
    private static final String USER_DELETED_TOPIC = "user.deleted";

    private static final Logger logger = LogManager.getLogger(UserEventListener.class);

    @Autowired
    public UserEventListener(UserEventRepository userEventRepository) {
        this.userEventRepository = userEventRepository;
    }

    @KafkaListener(topics = USER_CREATED_TOPIC, groupId = "reservation-service")
    public void handleUserCreated(UserEvent userEvent) {
        logger.info("Received user created event: {}", userEvent);
        try {
            userEventRepository.save(userEvent);
            logger.info("Successfully saved user created event: {}", userEvent);
        } catch (Exception e) {
            logger.error("Error while saving user created event: {}", userEvent, e);
        }
    }

    @KafkaListener(topics = USER_UPDATED_TOPIC, groupId = "reservation-service")
    public void handleUserUpdated(UserEvent userEvent) {
        logger.info("Received user updated event: {}", userEvent);
        // Handle user updated event
        try {
            userEventRepository.save(userEvent);
            logger.info("Successfully saved user updated event: {}", userEvent);
        } catch (Exception e) {
            logger.error("Error while saving user updated event: {}", userEvent, e);
        }
    }

    @KafkaListener(topics = USER_DELETED_TOPIC, groupId = "reservation-service")
    public void handleUserDeleted(UserEvent userEvent) {
        logger.info("Received user deleted event: {}", userEvent);
        // Handle user deleted event
        try {
            userEventRepository.delete(userEvent);
            logger.info("Successfully deleted user event: {}", userEvent);
        } catch (Exception e) {
            logger.error("Error while deleting user event: {}", userEvent, e);
        }
    }
}
