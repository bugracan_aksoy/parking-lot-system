package org.andersenlab.reservationservice.kafka;

import org.andersenlab.reservationservice.model.Reservation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/* bugracanaksoy created on 29.05.2024 */
@Service
public class ReservationEventProducer {

    @Value("${kafka.topic.reservation.created}")
    private String RESERVATION_CREATED_TOPIC;

    @Value("${kafka.topic.reservation.updated}")
    private String RESERVATION_UPDATED_TOPIC;

    @Value("${kafka.topic.reservation.deleted}")
    private String RESERVATION_DELETED_TOPIC;

    private final KafkaTemplate<String, Object> kafkaTemplate;

    private static final Logger logger = LogManager.getLogger(ReservationEventProducer.class);

    @Autowired
    public ReservationEventProducer(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendReservationCreatedEvent(Reservation reservation) {
        logger.info("Sending reservation created event for reservation: {}", reservation);
        try {
            kafkaTemplate.send(RESERVATION_CREATED_TOPIC, reservation);
            logger.info("Successfully sent reservation created event for reservation: {}", reservation);
        } catch (Exception e) {
            logger.error("Error while sending reservation created event for reservation: {}", reservation, e);
        }
    }

    public void sendReservationUpdatedEvent(Reservation reservation) {
        logger.info("Sending reservation updated event for reservation: {}", reservation);
        try {
            kafkaTemplate.send(RESERVATION_UPDATED_TOPIC, reservation);
            logger.info("Successfully sent reservation updated event for reservation: {}", reservation);
        } catch (Exception e) {
            logger.error("Error while sending reservation updated event for reservation: {}", reservation, e);
        }
    }

    public void sendReservationDeletedEvent(Long reservationId) {
        logger.info("Sending reservation deleted event for reservation ID: {}", reservationId);
        try {
            kafkaTemplate.send(RESERVATION_DELETED_TOPIC, reservationId);
            logger.info("Successfully sent reservation deleted event for reservation ID: {}", reservationId);
        } catch (Exception e) {
            logger.error("Error while sending reservation deleted event for reservation ID: {}", reservationId, e);
        }
    }
}
