package org.andersenlab.reservationservice.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

/* bugracanaksoy created on 8.07.2024 */
@Configuration
public class KafkaTopicConfig {

    @Value("${kafka.topic.reservation.created}")
    private String RESERVATION_CREATED_TOPIC;

    @Value("${kafka.topic.reservation.updated}")
    private String RESERVATION_UPDATED_TOPIC;

    @Value("${kafka.topic.reservation.deleted}")
    private String RESERVATION_DELETED_TOPIC;

    @Bean
    public NewTopic reservationCreatedTopic() {
        return TopicBuilder.name(RESERVATION_CREATED_TOPIC)
                .partitions(3)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic reservationUpdatedTopic() {
        return TopicBuilder.name(RESERVATION_UPDATED_TOPIC)
                .partitions(3)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic reservationDeletedTopic() {
        return TopicBuilder.name(RESERVATION_DELETED_TOPIC)
                .partitions(3)
                .replicas(1)
                .build();
    }
}
