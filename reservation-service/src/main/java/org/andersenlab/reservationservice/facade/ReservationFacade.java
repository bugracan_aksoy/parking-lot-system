package org.andersenlab.reservationservice.facade;

import org.andersenlab.reservationservice.model.Reservation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/* bugracanaksoy created on 15.07.2024 */
public interface ReservationFacade {
    Reservation createReservation(Reservation reservation);

    Reservation getReservationById(Long reservationId);

    Page<Reservation> getAllReservations(Pageable pageable);

    Reservation updateReservation(Long reservationId, Reservation reservation);

    void deleteReservation(Long reservationId);
}

