package org.andersenlab.reservationservice.facade.impl;

import org.andersenlab.reservationservice.facade.ReservationFacade;
import org.andersenlab.reservationservice.model.Reservation;
import org.andersenlab.reservationservice.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/* bugracanaksoy created on 15.07.2024 */
@Service
public class ReservationFacadeImpl implements ReservationFacade {
    private final ReservationService reservationService;

    @Autowired
    public ReservationFacadeImpl(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @Override
    public Reservation createReservation(Reservation reservation) {
        return reservationService.createReservation(reservation);
    }

    @Override
    public Reservation getReservationById(Long reservationId) {
        return reservationService.getReservationById(reservationId);
    }

    @Override
    public Page<Reservation> getAllReservations(Pageable pageable) {
        return reservationService.getAllReservations(pageable);
    }

    @Override
    public Reservation updateReservation(Long reservationId, Reservation reservation) {
        reservation.setId(reservationId);
        return reservationService.updateReservation(reservation);
    }

    @Override
    public void deleteReservation(Long reservationId) {
        reservationService.deleteReservation(reservationId);
    }
}

