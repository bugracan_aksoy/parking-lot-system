package org.andersenlab.reservationservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.andersenlab.reservationservice.facade.ReservationFacade;
import org.andersenlab.reservationservice.model.Reservation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

/* bugracanaksoy created on 29.05.2024 */
@RestController
@RequestMapping("/reservations")
@Tag(name = "Reservation Management System", description = "Operations pertaining to reservation management")
public class ReservationController {
    private final ReservationFacade reservationFacade;
    private static final Logger logger = LogManager.getLogger(ReservationController.class);

    @Autowired
    public ReservationController(ReservationFacade reservationFacade) {
        this.reservationFacade = reservationFacade;
    }

    @Operation(summary = "Create a new reservation", description = "Create a new reservation and return the created reservation")
    @PostMapping
    public ResponseEntity<?> createReservation(@Parameter(description = "Reservation object to be created", required = true) @RequestBody Reservation reservation) {
        logger.info("Received request to create reservation: {}", reservation);
        try {
            Reservation createdReservation = reservationFacade.createReservation(reservation);
            logger.info("Successfully created reservation: {}", createdReservation);
            return ResponseEntity.ok(createdReservation);
        } catch (UsernameNotFoundException e) {
            logger.error("Error creating reservation: {}", reservation, e);
            return ResponseEntity.status(500).body("User is not available");
        } catch (Exception e) {
            logger.error("Error creating reservation: {}", reservation, e);
            return ResponseEntity.status(500).body(null);
        }
    }

    @Operation(summary = "Get reservation by ID", description = "Retrieve reservation details by reservation ID")
    @GetMapping("/{reservationId}")
    public ResponseEntity<Reservation> getReservationById(@Parameter(description = "ID of the reservation to retrieve", required = true) @PathVariable Long reservationId) {
        logger.info("Received request to get reservation by ID: {}", reservationId);
        try {
            Reservation reservation = reservationFacade.getReservationById(reservationId);
            if (reservation != null) {
                logger.info("Successfully retrieved reservation: {}", reservation);
                return ResponseEntity.ok(reservation);
            } else {
                logger.warn("Reservation not found with ID: {}", reservationId);
                return ResponseEntity.status(404).body(null);
            }
        } catch (Exception e) {
            logger.error("Error retrieving reservation by ID: {}", reservationId, e);
            return ResponseEntity.status(500).body(null);
        }
    }

    @Operation(summary = "Get all reservations with pagination", description = "Retrieve all reservations with pagination support")
    @GetMapping
    public ResponseEntity<Page<Reservation>> getAllReservations(
            @Parameter(description = "Page number for pagination", example = "0") @RequestParam(defaultValue = "0") int page,
            @Parameter(description = "Page size for pagination", example = "10") @RequestParam(defaultValue = "10") int size) {
        logger.info("Received request to get all reservations");
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Reservation> reservations = reservationFacade.getAllReservations(pageable);
            logger.info("Successfully retrieved all reservations");
            return ResponseEntity.ok(reservations);
        } catch (Exception e) {
            logger.error("Error retrieving all reservations", e);
            return ResponseEntity.status(500).body(null);
        }
    }

    @Operation(summary = "Update an existing reservation", description = "Update reservation details by reservation ID")
    @PutMapping("/{reservationId}")
    public ResponseEntity<Reservation> updateReservation(@Parameter(description = "ID of the reservation to be updated", required = true) @PathVariable Long reservationId, @Parameter(description = "Updated reservation object", required = true) @RequestBody Reservation reservation) {
        logger.info("Received request to update reservation with ID: {}", reservationId);
        try {
            Reservation updatedReservation = reservationFacade.updateReservation(reservationId, reservation);
            logger.info("Successfully updated reservation: {}", updatedReservation);
            return ResponseEntity.ok(updatedReservation);
        } catch (Exception e) {
            logger.error("Error updating reservation with ID: {}", reservationId, e);
            return ResponseEntity.status(500).body(null);
        }
    }

    @Operation(summary = "Delete reservation by ID", description = "Delete a reservation by ID")
    @DeleteMapping("/{reservationId}")
    public ResponseEntity<Void> deleteReservation(@Parameter(description = "ID of the reservation to delete", required = true) @PathVariable Long reservationId) {
        logger.info("Received request to delete reservation with ID: {}", reservationId);
        try {
            reservationFacade.deleteReservation(reservationId);
            logger.info("Successfully deleted reservation with ID: {}", reservationId);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            logger.error("Error deleting reservation with ID: {}", reservationId, e);
            return ResponseEntity.status(500).build();
        }
    }
}
